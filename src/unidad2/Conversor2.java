package unidad2;

import java.io.Console;

public class Conversor2 {

	/*
	 * Conversor de euros a dólares. El programa lee la cantidad en euros por teclado y muestra
	 * en la pantalla la conversión a dolares con una precisión de 2 decimales.
	 * La entrada de datos se realiza con la clase Console (para que funcione se ha de ejecutar
	 * desde la línea de comando).
	 */
	
	public static void main(String[] args) {
		System.out.println("Conversor Euros/Dolares");
		Console in = System.console();
		System.out.print("Euros: ");
		float euros = Float.parseFloat(in.readLine());
		System.out.print("Tipo de cambio Euros/Dolares: ");
		float tipo = Float.parseFloat(in.readLine());
		System.out.printf("Dolares: %.2f\n", euros * tipo);
	}
	
}
