package unidad2;

public class Division {

	/* 
	 * Programa que muestre en la consola la división real de 1234 entre 532. El formato
	 * de salida será un número que ocupará un mínimo de 15 caracteres en pantalla, de los
	 * cuales dos se utilizarán para la parte decimal.
	 */
	
	public static void main(String[] args) {
		System.out.printf("1234 / 532 = %15.2f", 1234d / 523d);
	}

}
