package unidad2;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class Conversor1 {

	/*
	 * Conversor de euros a dólares. El programa lee la cantidad en euros por teclado y muestra 
	 * en la pantalla la conversión a dolares con una precisión de 2 decimales.
	 * La entrada de datos se realiza con la clase BufferedReader.
	 */
	
	
	public static void main(String[] args) throws IOException {
		System.out.println("Conversor Euros/Dolares");
		BufferedReader in = new BufferedReader(new InputStreamReader(System.in));
		System.out.print("Euros: ");
		float euros = Float.parseFloat(in.readLine());
		System.out.print("Tipo de cambio Euros/Dolares: ");
		float tipo = Float.parseFloat(in.readLine());
		System.out.printf("Dolares: %.2f\n", euros * tipo);
	}
	
}
